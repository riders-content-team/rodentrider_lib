#!/usr/bin/env python
from robot_control import RobotControl
import rospy
from std_srvs.srv import Trigger, TriggerRequest
import math
from std_msgs.msg import String
import time
from angles import normalize_angle
import os
import requests
from rodentrider_arena.srv import RemoveModel
class Robot:
    def __init__(self, robot_name):

        rospy.init_node("rodentrider_controller")
        self.robot_pose_pub = rospy.Publisher("/robot_pose/", String, queue_size=10)

        robot_data = {
        "robot_name": robot_name,
        "base_link": "chassis",
        }

        pos_data = {
        "x": (-4.5),
        "y": (-4.5),
        "z": (0),
        "yaw": (math.pi/2)
        }

        event_data = {
        "unit_length": (1),
        "event_period": (1)
        }

        self.robot_name = robot_name
        self.pos_x = pos_data['x']
        self.pos_y = pos_data['y']
        self.dir = pos_data['yaw']

        self.speed_scale = 3

        self.robot = RobotControl(robot_data, pos_data, event_data)

        self.map = [
            1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
            1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0, 1,
            1, 0, 1, 0, 1, 1, 1, 1, 1, 1, 0, 0, 1, 0, 1, 0, 0, 1, 0, 1,
            1, 0, 1, 0, 1, 0, 0, 0, 0, 1, 0, 1, 1, 0, 1, 1, 1, 1, 0, 1,
            1, 0, 0, 0, 1, 0, 1, 1, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 1,
            1, 1, 1, 0, 1, 0, 0, 1, 0, 1, 0, 1, 1, 0, 1, 1, 1, 1, 1, 1,
            1, 0, 0, 0, 1, 1, 1, 1, 0, 1, 0, 1, 0, 0, 1, 0, 0, 0, 0, 1,
            1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 1, 0, 1, 1, 0, 1,
            1, 0, 1, 0, 0, 0, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0, 0, 1, 0, 1,
            1, 0, 1, 0, 1, 1, 1, 1, 0, 1, 0, 1, 1, 1, 1, 1, 1, 1, 0, 1,
            1, 0, 1, 0, 1, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 1,
            1, 0, 1, 0, 1, 0, 0, 0, 0, 1, 0, 1, 0, 0, 1, 1, 1, 1, 0, 1,
            1, 0, 1, 0, 1, 0, 1, 1, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1,
            1, 0, 0, 0, 1, 0, 0, 1, 0, 1, 1, 1, 0, 0, 1, 0, 1, 0, 0, 1,
            1, 0, 1, 1, 1, 0, 0, 1, 0, 1, 0, 0, 0, 0, 1, 0, 1, 1, 0, 1,
            1, 1, 1, 0, 1, 1, 1, 1, 0, 1, 0, 0, 0, 0, 1, 1, 1, 0, 0, 1,
            1, 0, 1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1,
            1, 0, 0, 0, 1, 1, 0, 0, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 0, 1,
            1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 1,
            1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
        ]
        self.map_width = 20

        self.pos_on_map_x = int(self.pos_x + 9.5)
        self.pos_on_map_y = int(9.5 - self.pos_y)

        self._init_game_controller()


    def _init_game_controller(self):
        try:
            rospy.wait_for_service("/robot_handler", 1.0)

            time.sleep(0.1)

            self.robot_handle = rospy.ServiceProxy('robot_handler', Trigger)
            resp = self.robot_handle()
            self.status = resp.message

            if not self.status == "no_robot":
                self.robot_pose = self.robot_name  + ":" + str(self.pos_x) + ":" + str(self.pos_y)
                self.robot_pose_pub.publish(self.robot_pose)

        except (rospy.ServiceException, rospy.ROSException), e:
            self.status = "free_roam"

            self.robot_pose = self.robot_name  + ":" + str(self.pos_x) + ":" + str(self.pos_y)
            self.robot_pose_pub.publish(self.robot_pose)

            rospy.logerr("Service call failed: %s" % (e,))

        try:
            rospy.wait_for_service("/remove_model", 5.0)
            self.remove_model = rospy.ServiceProxy('remove_model', RemoveModel)

        except (rospy.ServiceException, rospy.ROSException), e:
            rospy.logerr("Service call failed: %s" % (e,))

    def check_game_controller(self):
        try:
            rospy.wait_for_service("/robot_handler", 5.0)

            self.robot_handle = rospy.ServiceProxy('robot_handler', Trigger)
            resp = self.robot_handle()

            self.status = resp.message
        except (rospy.ServiceException, rospy.ROSException), e:
            self.status = "stop"
            rospy.logerr("Service call failed: %s" % (e,))


    def move_forward(self):
        if not self.status == "stop" and not self.status == "no_robot":

            if not self.wall_front:
                self.pos_x, self.pos_y, self.dir = self.robot.move_distance(1, self.speed_scale)

                self.robot_pose = self.robot_name  + ":" + str(self.pos_x) + ":" + str(self.pos_y)
                self.robot_pose_pub.publish(self.robot_pose)

                self.pos_on_map_x = int(self.pos_x + 9.5)
                self.pos_on_map_y = int(9.5 - self.pos_y)


    def turn_left(self):
        if not self.status == "stop" and not self.status == "no_robot":
            self.pos_x, self.pos_y, self.dir = self.robot.rotate_angle(math.pi/2, self.speed_scale)

            self.robot_pose = self.robot_name  + ":" + str(self.pos_x) + ":" + str(self.pos_y)
            self.robot_pose_pub.publish(self.robot_pose)

            self.pos_on_map_x = int(self.pos_x + 9.5)
            self.pos_on_map_y = int(9.5 - self.pos_y)


    def turn_right(self):
        if not self.status == "stop" and not self.status == "no_robot":
            self.pos_x, self.pos_y, self.dir = self.robot.rotate_angle(-math.pi/2, self.speed_scale)

            self.robot_pose = self.robot_name  + ":" + str(self.pos_x) + ":" + str(self.pos_y)
            self.robot_pose_pub.publish(self.robot_pose)

            self.pos_on_map_x = int(self.pos_x + 9.5)
            self.pos_on_map_y = int(9.5 - self.pos_y)


    def remove_wall(self):
        self.remove_model("wall")


    @property
    def wall_front(self):

        if abs(normalize_angle(self.dir)) < 0.01 and self.map[(self.pos_on_map_y) * self.map_width + (self.pos_on_map_x + 1)]:
            return True
        elif abs(normalize_angle(self.dir - math.pi/2)) < 0.01 and self.map[(self.pos_on_map_y - 1) * self.map_width + (self.pos_on_map_x)]:
            return True
        elif abs(normalize_angle(self.dir - math.pi)) < 0.01 and self.map[(self.pos_on_map_y) * self.map_width + (self.pos_on_map_x - 1)]:
            return True
        elif abs(normalize_angle(self.dir + math.pi/2)) < 0.01 and self.map[(self.pos_on_map_y + 1) * self.map_width + (self.pos_on_map_x)]:
            return True

        return False

    @property
    def wall_left(self):
        
        if abs(normalize_angle(self.dir)) < 0.01 and self.map[(self.pos_on_map_y - 1) * self.map_width + (self.pos_on_map_x)]:
            return True
        elif abs(normalize_angle(self.dir - math.pi/2)) < 0.01 and self.map[(self.pos_on_map_y) * self.map_width + (self.pos_on_map_x - 1)]:
            return True
        elif abs(normalize_angle(self.dir - math.pi)) < 0.01 and self.map[(self.pos_on_map_y + 1) * self.map_width + (self.pos_on_map_x)]:
            return True
        elif abs(normalize_angle(self.dir + math.pi/2)) < 0.01 and self.map[(self.pos_on_map_y) * self.map_width + (self.pos_on_map_x + 1)]:
            return True

        return False

    @property
    def wall_right(self):
        
        if abs(normalize_angle(self.dir)) < 0.01 and self.map[(self.pos_on_map_y + 1) * self.map_width + (self.pos_on_map_x)]:
            return True
        elif abs(normalize_angle(self.dir - math.pi/2)) < 0.01 and self.map[(self.pos_on_map_y) * self.map_width + (self.pos_on_map_x + 1)]:
            return True
        elif abs(normalize_angle(self.dir - math.pi)) < 0.01 and self.map[(self.pos_on_map_y - 1) * self.map_width + (self.pos_on_map_x)]:
            return True
        elif abs(normalize_angle(self.dir + math.pi/2)) < 0.01 and self.map[(self.pos_on_map_y) * self.map_width + (self.pos_on_map_x - 1)]:
            return True

        return False

    def is_ok(self):
        if not rospy.is_shutdown():
            rospy.sleep(0.05)
            return True
        else:
            return False
