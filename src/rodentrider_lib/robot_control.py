#!/usr/bin/env python

from __future__ import division
import rospy
from conversions import Vector3, Quaternion, EulerAngles
from gazebo_msgs.srv import GetLinkState
from gazebo_msgs.msg import ModelState
from gazebo_msgs.msg import LinkState
from std_srvs.srv import Trigger, TriggerRequest
import math
import sys
from std_msgs.msg import String
from angles import normalize_angle
import time

class RobotControl:

    def __init__(self, robot_data, initial_pos, event_data):

        self.robot_name = robot_data['robot_name']
        self.base_link = robot_data['base_link']

        # N=>pi/2 E=>0 S=>3*pi/2 W=>pi
        self.dir = initial_pos['yaw']
        self.pos_x = initial_pos['x']
        self.pos_y = initial_pos['y']
        self.pos_z = initial_pos['z']

        self.prev_dir = self.dir
        self.prev_pos_x = self.pos_x
        self.prev_pos_y = self.pos_y

        self.unit_length = event_data['unit_length']
        self.event_period = event_data['event_period']
        self.msg_rate = 100

        self.set_model_state = rospy.Publisher("/gazebo/set_model_state", ModelState, queue_size=10)
        self.set_link_state = rospy.Publisher("/gazebo/set_link_state", LinkState, queue_size=10)

        time.sleep(1)
        self.update(self.pos_x, self.pos_y, self.dir)


    def update(self, x, y, heading):
        q = Quaternion()
        q.euler_to_quaternion(0, 0, heading)

        state_msg = ModelState()
        state_msg.model_name = self.robot_name
        state_msg.pose.position.x = x
        state_msg.pose.position.y = y
        state_msg.pose.position.z = self.pos_z
        state_msg.pose.orientation.x = q.x
        state_msg.pose.orientation.y = q.y
        state_msg.pose.orientation.z = q.z
        state_msg.pose.orientation.w = q.w
        self.set_model_state.publish(state_msg)

    def polynomial_coef(self, initial_pos, final_pos, final_time, initial_vel = 0, final_vel = 0):

        a0 = initial_pos
        a1 = initial_vel
        a2 = (3/(final_time**2))*(final_pos - initial_pos) - (2/final_time)*initial_vel - final_vel/final_time
        a3 = -(2/(final_time**3))*(final_pos - initial_pos) +  (final_vel - initial_vel)/final_time**2

        return a0, a1, a2, a3

    def pos_on_t(self, t, a0, a1, a2, a3):

        pos = a3*t**3 + a2*t**2 + a1*t + a0
        vel = 3*a3*t**2 + 2*a2*t + a1
        acc = 6*a3*t + 2*a2

        return pos, vel

    def move_to_point(self, unit, speed_scale):

        move_period = self.event_period*abs(unit)

        a0x, a1x, a2x, a3x = self.polynomial_coef(self.prev_pos_x, self.pos_x, move_period)
        a0y, a1y, a2y, a3y = self.polynomial_coef(self.prev_pos_y, self.pos_y, move_period)
        self.prev_pos_x = self.pos_x
        self.prev_pos_y = self.pos_y
        t = 0

        while move_period - t >= 0.05:
            posx, velx = self.pos_on_t(t,a0x, a1x, a2x, a3x)
            posy, vely = self.pos_on_t(t,a0y, a1y, a2y, a3y)

            vel = velx
            if vel == 0:
                vel = vely

            t = t + (speed_scale / self.msg_rate)
            self.update(posx, posy, self.dir)

            rate = rospy.Rate(self.msg_rate)
            rate.sleep()

    def rotate(self, angle, speed_scale):

        move_period = self.event_period*abs(angle)/(math.pi/2)

        error = self.dir - self.prev_dir

        if error > math.pi:
            self.prev_dir = self.prev_dir + math.pi*2
        elif error < -math.pi:
            self.prev_dir = self.prev_dir - math.pi*2

        a0, a1, a2, a3 = self.polynomial_coef(self.prev_dir
        , normalize_angle(self.dir), move_period)
        self.prev_dir = self.dir
        t = 0

        while move_period - t >= 0.05:
            pos, vel = self.pos_on_t(t,a0, a1, a2, a3)

            t = t + (speed_scale / self.msg_rate)
            self.update(self.pos_x, self.pos_y, pos)
            rate = rospy.Rate(self.msg_rate)
            rate.sleep()

    def move_distance(self, unit, speed_scale):
        if abs(normalize_angle(self.dir - math.pi/2)) < 0.01:
            self.pos_y += self.unit_length*unit
        elif abs(normalize_angle(self.dir)) < 0.01:
            self.pos_x += self.unit_length*unit
        elif abs(normalize_angle(self.dir - math.pi)) < 0.01:
            self.pos_x -= self.unit_length*unit
        elif abs(normalize_angle(self.dir + math.pi/2)) < 0.01:
            self.pos_y -= self.unit_length*unit

        self.move_to_point(unit, max(1,min(10,speed_scale)))

        return self.pos_x, self.pos_y, self.dir

    def rotate_angle(self, angle, speed_scale):
        self.dir += angle
        self.dir = normalize_angle(self.dir)

        self.rotate(angle, max(1,min(10,speed_scale)))

        return self.pos_x, self.pos_y, self.dir

