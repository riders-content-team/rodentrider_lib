#!/usr/bin/env python
from rodentrider_lib.rodentrider import Robot
import rospy
import math
import time

robot = Robot("rodentrider")

while robot.is_ok():
    if robot.wall_left == False:
        robot.turn_left()
        robot.move_forward()
    elif robot.wall_front == False:
        robot.move_forward()
    else:
        robot.turn_right()